=======
daa-uit
=======

Python script to login into DAA_ from the cli.
It is useful if you want to sign up automatically for subjects in DAA_.

.. _DAA: https://dkhp.uit.edu.vn/

Status
------

Incompleted.

Dependencies
------------

.. code-block:: bash

   sudo apt-get install python-future python-requests python-lxml

Compability
-----------

This script support Python 2.7+ and Python 3.5+.

Usage
------------

.. code-block:: bash

   $ python main.py
   ===============================
   # Root-me login script by You #
   ==============================#
   Login: example@example.com
   Password:

