#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

from builtins import input, range, bytes

## System import
import getpass # Portable password input
import logging
import sys

## Third party
import requests
import lxml.html

## Relative import
from cookiebake import *

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
)

log = logging.getLogger(__name__)

def input_login():
    login = input("Login: ")
    return login

def input_password():
    passwd = getpass.getpass('Password: ')
    return passwd

def request(method, url, data=None, params=None, repeat=10):
    try:
        log.info("%s %r ..." % (method.__func__.__name__.upper(), url))
        for n_try in range(repeat):
            req = method(url, data=data, params=params, timeout=4)
            if req.status_code == requests.codes.ok:
                return req
            log.warning("Trying %d times"%(n_try + 1))

        log.error('Cannot connect! Code: %s' % (req.status_code))
    except (requests.exceptions.ReadTimeout, requests.exceptions.ConnectionError):
        log.error("ConnectionError")

def write_html(content, filename):
    with open(filename, 'wb') as fd:
        fd.write(content)

def get_value_from_name(tree, name):
    """get_value_from_name(tree, name) -> str

    Return value attribute from name attribute of an input tag.
    """
    assert isinstance(tree, lxml.html.HtmlElement)
    elements = tree.xpath("//input[@name='%(name)s']"%locals())
    return elements[0].value

class UIT():
    """This class provide way to course registration in https://dkhp.uit.edu.vn/"""
    def __init__(self, student_id, passwd):
        self.mssv = student_id
        self.passwd = passwd

        self.session = requests.Session()
        self.session.headers.update(UIT.HEADERS)

        self.login_data = {
            'name': self.mssv,   # mssv
            'pass': self.passwd, # passwd
            'form_build_id': None,
            'form_id': 'user_login',
            'op': 'Log in',
            }

        self.register_data = {
            "dsmalop": "\r\n".join(UIT.DSMALOP),
            "form_build_id": None,
            "form_id": "uit_dkhp_dangky_form",
            'form_token': None,
            "op": u"Đăng ký",
            "txtmasv": None, # masv
            }

    def _get_request(self, url, params=None):
        r = request(self.session.get, url, params=params)
        return r

    def _login(self, tree):
        """_login(self, tree) -> requests.models.Response

        Send login_data and save cookies if succeed.
        Return a respone from that post request.
        """
        self.login_data["form_build_id"] = get_value_from_name(tree, "form_build_id")

        r = request(self.session.post, UIT.URL_LOGIN, data=self.login_data)

        if self.session.cookies._cookies:
            log.info('Login success ...')
            log.info("Saving cookies in %r ..." % UIT.COOKIE_FILE)
            cookies_save(self.session, UIT.COOKIE_FILE)
        else:
            write_html(r.content, UIT.DEBUG_OUTPUT_HTML)
            log.info('status_code: %d' % r.status_code)
            log.info('URL: %r' % r.url)
            log.error('Something wrong! Cannot connect!')
        return r

    def login(self):
        """login(self) -> lxml.html.HtmlElement

        Return html element of the succeed login requests
        """
        r = self._get_request(UIT.URL_LOGIN)
        loaded = cookies_load(self.session, UIT.COOKIE_FILE)

        if loaded and not cookies_expired(self.session, UIT._COOKIE_DOMAIN, UIT._COOKIE_JAR):
            log.info("Use previous cookies from %r" % UIT.COOKIE_FILE)
        else:
            tree = lxml.html.fromstring(r.content)
            log.info("Try to log in %r" % UIT.URL_LOGIN)
            r = self._login(tree)

        if r.url != UIT.URL_DKHP:
            log.info("Current URL: %r" % r.url)
            log.info("Redirecting to %r ..." % UIT.URL_DKHP)
            r = request(self.session.get, UIT.URL_DKHP)

        if r.status_code == 403:
            log.error("Trying again at 9.00 am on 07/12/2017")

        tree = lxml.html.fromstring(r.content)
        return tree

    def submit(self, tree):
        self.register_data['form_token'] = get_value_from_name(tree, 'form_token')
        self.register_data['form_build_id'] = get_value_from_name(tree, 'form_build_id')

        ## post our danhsach dkhp
        log.info('[+] Trying to sign up subject ...')
        for count in range(100):
            try:
                log.info("Trying %dth times" % count)
                r = self.session.post(UIT.URL_DKHP, data=self.register_data, timeout=5.0)
                content = r.content
                if u'LỚP HỌC ĐÃ ĐĂNG KÝ' in content:
                    write_html(content)
                    print("YEAH, YOU JUST DO IT MAN !")
                    break
                if 'try again later' in content:
                    continue
            except requests.exceptions.ReadTimeout:
                pass


    HEADERS = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        # "Referer": "https://dkhp.uit.edu.vn/",
        # "Cookie": "has_js=0",
        "Connection": "keep-alive",
        }

    DSMALOP = [
        'NT114.I21.ANTT', 'NT119.I21.ANTT', 'NT119.I21.ANTT.1', 'NT130.I21.ANTT',
        'NT130.I21.ANTT.1', 'NT330.I21', 'NT330.I21.1', 'NT532.I21', 'NT532.I21.1',
        'NT534.I21.ANTT', 'NT534.I21.ANTT.1',
        ]

    COOKIE_FILE  = '.dkhp_cookies.json'

    URL_LOGIN    = 'https://dkhp.uit.edu.vn/'
    URL_DANHSACH = 'https://dkhp.uit.edu.vn/sinhvien/hieuchinh/danhsach'
    URL_LOGOUT   = 'https://dkhp.uit.edu.vn/user/logout'
    URL_DKHP     = 'https://dkhp.uit.edu.vn/sinhvien/hocphan/dangky'

    DEBUG_OUTPUT_HTML = 'dkhp_daa.html'
    _COOKIE_DOMAIN = '.dkhp.uit.edu.vn'
    _COOKIE_JAR = 'SESS9e6589ddccce770da5ff5eb24cc51252'


def main():
    print("""\
===============================
# DAA UIT login script by You #
==============================#""")
    student_id = input_login()
    password = input_password()

    uit = UIT(student_id, password)
    uit.login()


if __name__ == '__main__':
    main()
